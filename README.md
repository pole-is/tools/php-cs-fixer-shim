# irstea/php-cs-fixer-shim - shim repository for friendsofphp/php-cs-fixer.

This package is a drop-in replacement for [friendsofphp/php-cs-fixer](https://packagist.org/packages/friendsofphp/php-cs-fixer), which provides its PHAR archive as a binary.

It is built automatically from the official PHAR.

## Installation

	composer require irstea/php-cs-fixer-shim

or:

	composer require --dev irstea/php-cs-fixer-shim



## Usage

As you would use the original package, i.e. something like:

	vendor/bin/php-cs-fixer [options] [arguments]

## License

This distribution retains the license of the original software: MIT